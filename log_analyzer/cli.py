from __future__ import annotations

import argparse
import contextlib
import logging
import sys
import typing

import importlib_metadata

from . import (
    analyze,
    constants,
    convert,
    errors,
    logfile,
)


_logger = logging.getLogger(__name__)


def main() -> None:
    argparser = create_argparser()
    args = argparser.parse_args()

    setup_logging(args.log_level.upper())
    _logger.debug('Log analyzer configured. Running..')

    try:
        args.main(args)

    except errors.LogAnalyzerFailure as err:
        _logger.error('Processing failed: %s', err)
        sys.exit(1)


def create_argparser() -> argparse.ArgumentParser:
    argparser = argparse.ArgumentParser('Log Analyzer')

    def print_help(args: argparse.Namespace) -> None:  # pylint: disable=unused-argument
        argparser.print_help()
        sys.exit(1)

    argparser.set_defaults(main=print_help)

    argparser.add_argument(
        '--version',
        action='version',
        version=importlib_metadata.version('msg-log-analyzer'),
    )

    LOG_LEVELS = (
        logging.WARNING,
        logging.INFO,
        logging.DEBUG,
    )

    argparser.add_argument(
        '--log-level',
        choices=[
            logging.getLevelName(log_level).lower()
            for log_level in LOG_LEVELS
        ],
        default=logging.getLevelName(LOG_LEVELS[0]).lower(),
        help='Set log level. Default: %(default)s',
    )

    subparsers = argparser.add_subparsers(title='Command')
    add_stats_arguments(subparsers.add_parser('stats', help='Show statistics from a log file'))
    add_convert_arguments(subparsers.add_parser('convert', help='Convert log file'))

    return argparser


def add_stats_arguments(argparser: argparse.ArgumentParser) -> None:
    argparser.set_defaults(main=stats_main)

    argparser.add_argument(
        'log_file',
        help='Path to log file'
    )


def stats_main(args: argparse.Namespace) -> None:
    stats = analyze.get_stats(args.log_file)

    print(args.log_file)
    print(f'  Messages: {stats.message_count}')
    print(f'  Invalid log entries: {stats.invalid_log_entry_count}')

    print('  Message type counts:')
    for message_type, message_count in stats.message_counts_by_type.items():
        print(f'    {message_type: <20} {message_count: >6}')


def add_convert_arguments(argparser: argparse.ArgumentParser) -> None:
    argparser.set_defaults(main=convert_main)

    argparser.add_argument(
        '--extract-containers',
        help='Extract messages from container messages',
        default=False,
        action='store_true',
    )

    argparser.add_argument(
        '--preserve-containers',
        help='Preserve container messages when extracting messages',
        default=False,
        action='store_true',
    )

    argparser.add_argument(
        '--annotate-extracted',
        help='Annotate boundaries of extracted container messages',
        default=False,
        action='store_true',
    )

    argparser.add_argument(
        '--filter-messages',
        dest='message_types',
        help='Only include messages of given types',
        default=[],
        action='append',
    )

    argparser.add_argument(
        '--filter-messagers',
        dest='messagers',
        help='Only include messages sent by or sent to the given messagers',
        default=set(),
        action=MessagerFilterAction,
    )

    argparser.add_argument(
        'source_path',
        help='Path to source log file'
    )

    argparser.add_argument(
        '--output',
        '-o',
        help='Path to file to write the result. If not set, the result is written to stdout'
    )


def convert_main(args: argparse.Namespace) -> None:
    original_entries = logfile.read_entries(args.source_path)

    entries = convert.convert(
        entries=original_entries,
        extract_containers=args.extract_containers,
        preserve_containers=args.preserve_containers,
        annotate_extracted=args.annotate_extracted,
        message_types=args.message_types,
        messagers=args.messagers,
    )

    with open_output_file(args.output) as output:
        logfile.save_as_json_log(entries, output)


@contextlib.contextmanager
def open_output_file(
    path: typing.Optional[str],
) -> typing.Generator[typing.TextIO, None, None]:
    if path:
        with open(path, 'wt', encoding=constants.LOG_FILE_ENCODING) as f:
            yield f

    else:
        yield sys.stdout


def setup_logging(log_level: str) -> None:
    handler = logging.StreamHandler(sys.stderr)

    handler.setFormatter(logging.Formatter(fmt='%(asctime)s %(message)s [%(name)s : %(lineno)s]'))

    logger = logging.getLogger(__package__)
    logger.propagate = False
    logger.addHandler(handler)
    logger.setLevel(log_level)


class MessagerFilterAction(argparse.Action):
    def __call__(self, parser, namespace, value, option_string=None):
        try:
            getattr(namespace, self.dest).add(int(value))

        except ValueError as err:
            raise argparse.ArgumentError(self, f'"{value}" is not a number') from err
