import setuptools


setuptools.setup(
    name='msg-log-analyzer',
    vcsver=True,
    packages=setuptools.find_packages(),
    entry_points={
        'console_scripts': [
            'log-analyzer = log_analyzer.cli:main',
        ],
    },
    install_requires=[
        'cerberus',
        'importlib-metadata',
    ],
    setup_requires=[
        'vcsver',
    ],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.9',
    ],
    python_requires='>=3.9',
)
