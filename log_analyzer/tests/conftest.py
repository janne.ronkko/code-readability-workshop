from __future__ import annotations

import functools
import os

import pytest


_TEST_LOG_DIR = os.path.dirname(__file__)


@pytest.fixture
def log_path():
    return functools.partial(
        os.path.join,
        _TEST_LOG_DIR,
    )
