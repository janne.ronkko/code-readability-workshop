from __future__ import annotations

import logging
import re
import typing

from .constants import TIMESTAMP_FORMAT
from . import types


_CONTAINER_INNER_MESSAGE_DATA_RE = re.compile(r'''
    (?P<message_type>[^\s]+)
    \s
    (?P<destination>\d+)
    \s
    (?P<sender>\d+)
    \s
    (?P<payload>.*)
''', flags=re.VERBOSE)

_logger = logging.getLogger(__name__)


def convert(
    entries: typing.Iterable[types.LogEntry],
    extract_containers: bool,
    preserve_containers: bool,
    annotate_extracted: bool,
    message_types: set[str],
    messagers: set[int],
) -> typing.Iterable[types.LogEntry]:
    if extract_containers:
        entries = extract_container_messages(
            entries,
            preserve_container=preserve_containers,
            annotate_extracted=annotate_extracted,
        )

    if message_types:
        entries = filter_by_messages(entries, message_types)

    if messagers:
        entries = filter_by_messagers(entries, messagers)

    return entries


def extract_container_messages(
    entries: typing.Iterable[types.LogEntry],
    *,
    preserve_container: bool = False,
    annotate_extracted: bool = False,
) -> typing.Generator[types.LogEntry, None, None]:
    for entry in entries:
        if entry.message_type == 'container':
            yield from expand_container_messages(
                entry,
                preserve_container=preserve_container,
                annotate_extracted=annotate_extracted,
            )

        else:
            yield entry


def expand_container_messages(
    container: types.LogEntry,
    *,
    preserve_container: bool = False,
    annotate_extracted: bool = False,
) -> typing.Generator[types.LogEntry, None, None]:
    if preserve_container:
        yield container

    if annotate_extracted:
        yield types.LogEntry(
            timestamp=container.timestamp,
            message_type='START_OF_CONTAINER',
            destination=container.destination,
            sender=container.sender,
            payload='Messages from container start here',
        )

    yield from extract_inner_messages(container)

    if annotate_extracted:
        yield types.LogEntry(
            timestamp=container.timestamp,
            message_type='END_OF_CONTAINER',
            destination=container.destination,
            sender=container.sender,
            payload='Messages from container ends here',
        )


def extract_inner_messages(
    container: types.LogEntry,
) -> typing.Generator[types.LogEntry, None, None]:
    assert container.message_type == 'container', container

    for message_data in container.payload.split('|||'):
        match = _CONTAINER_INNER_MESSAGE_DATA_RE.match(message_data)
        if not match:
            _logger.warning(
                'Container message sent at %s from %d to %d contains invalid message "%s"',
                container.timestamp.strftime(TIMESTAMP_FORMAT),
                container.sender,
                container.destination,
                message_data,
            )
            continue

        yield types.LogEntry(
            timestamp=container.timestamp,
            message_type=match.group('message_type'),
            destination=int(match.group('destination')),
            sender=int(match.group('sender')),
            payload=match.group('payload'),
        )


def filter_by_messages(
    log_entries: typing.Iterable[types.LogEntry],
    message_types: set[str],
) -> typing.Generator[types.LogEntry, None, None]:

    return (
        log_entry
        for log_entry in log_entries
        if log_entry.message_type in message_types
    )


def filter_by_messagers(
    log_entries: typing.Iterable[types.LogEntry],
    messagers: set[int],
) -> typing.Generator[types.LogEntry, None, None]:

    return (
        log_entry
        for log_entry in log_entries
        if log_entry.sender in messagers or log_entry.destination in messagers
    )
