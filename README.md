Setup and Activate Python Virtual Environment
=============================================

Setup Python virtual environment (https://docs.python.org/3/tutorial/venv.html):

```
make venv
```

For details about Python virtual enviroments read https://realpython.com/python-virtual-environments-a-primer/

Activate virtual environment (you need to do this every time you start a new terminal):

```
# On MacOS and Linux
source venv/bin/activate

# On Windows
venv\Scripts\activate.bat
```

Try The Application
===================

The example logs (input files) are in *example_logs* directory.

```
python -m log_analyzer stats example_logs/example1.log
python -m log_analyzer convert example_logs/example1.log -o example1.jsonlog
python -m log_analyzer stats example1.jsonlog
python -m log_analyzer convert example_logs/example1.log --extract-containers
python -m log_analyzer convert example1.jsonlog --extract-containers
```


Run Static Analyzers
====================

```
make mypy pycodestyle pylint
```

Run Tests
=========

Run all tests:

```
make pytest
```

```
make coverage-html-report
```

The command creates HTML report into _coverage_report_ directory; open index.html with your browser.


Update Used Tools
=================

Install pip-tools (https://github.com/jazzband/pip-tools)

```
pip install pip-tools
```


Update tools

```
make upgrade-depndencies
```

Note that if there was updates, the _requirements/development.txt_ tracked by git is updated, i.e. you
would commit the changes to that files (after testing that the new versions work with your code).
