from __future__ import annotations

import contextlib
import datetime
import io
import json
import logging
import os.path
import re
import typing

import cerberus

from . import (
    constants,
    errors,
    types,
)


_logger = logging.getLogger(__name__)


LogEntries = typing.Generator[types.LogEntry, None, None]


_LOG_FILE_FACTORIES: dict[
    str,
    typing.Callable[[typing.TextIO], LogFile]
] = {}


def register_logfile_format(extension):
    def factory_decorator(create):
        _LOG_FILE_FACTORIES[f'.{extension}'] = create

        return create

    return factory_decorator


def _get_log_file_factory(
    path: str,
) -> typing.Callable[[typing.TextIO], LogFile]:
    file_extension = os.path.splitext(path)[1]

    factory = _LOG_FILE_FACTORIES.get(file_extension)
    if not factory:
        raise errors.LogAnalyzerFailure(f'Unsupported log file {path}: the extension is unknown')

    return factory


def read_entries(path: str) -> LogEntries:
    with open(path) as log_file:
        yield from log_file


@contextlib.contextmanager
def open(  # pylint: disable=redefined-builtin
    path: str
) -> typing.Generator[LogFile, None, None]:
    create = _get_log_file_factory(path)

    try:
        fileobj = io.open(path, 'rt', encoding=constants.LOG_FILE_ENCODING)

    except FileNotFoundError as err:
        raise errors.LogAnalyzerFailure(f'The file {path} does not exist.') from err

    except PermissionError as err:
        raise errors.LogAnalyzerFailure(f'Can not read {path}.') from err

    with fileobj:
        yield create(fileobj)


def save_as_json_log(
    log_entries: typing.Iterable[types.LogEntry],
    output: typing.IO[str],
) -> None:
    for log_entry in log_entries:
        entry_data = {
            'timestamp': log_entry.timestamp.strftime(constants.TIMESTAMP_FORMAT),
            'message_type': log_entry.message_type,
            'destination': log_entry.destination,
            'sender': log_entry.sender,
            'payload': log_entry.payload,
        }

        json.dump(entry_data, output)
        output.write('\n')


class LogFile:
    def __init__(
        self,
        fileobj: typing.TextIO,
    ) -> None:
        super().__init__()

        self.invalid_log_entry_count = 0

        self._fileobj = fileobj

    def __iter__(self) -> LogEntries:
        for line in self._fileobj:
            line = line.rstrip('\n')

            try:
                yield self._parse_log_entry(line)

            except ValueError as err:
                _logger.warning('Failed to parse line: %s: %s', line, err)
                self.invalid_log_entry_count += 1

    def _parse_log_entry(self, entry_data: str) -> types.LogEntry:
        data = self._get_log_entry_data(entry_data)

        validator = cerberus.Validator(_LOG_ENTRY_DATA_SCHEMA)
        if not validator.validate(data):
            raise ValueError(validator.errors)

        validated_data = validator.document

        return types.LogEntry(
            timestamp=validated_data['timestamp'],
            message_type=validated_data['message_type'],
            destination=validated_data['destination'],
            sender=validated_data['sender'],
            payload=validated_data['payload'],
        )

    def _get_log_entry_data(self, entry_data: str) -> dict[str, typing.Any]:
        raise NotImplementedError()


@register_logfile_format('log')
class TextLogFile(LogFile):
    _ENTRY_RE = re.compile(
        r'\s*'
        r'(?P<timestamp>[^\s].*?[^s])'
        r' '
        r'(?P<message_type>[^\s]+)'
        r' '
        r'(?P<destination>\d+)'
        r' '
        r'(?P<sender>\d+)'
        r' '
        r'(?P<payload>.*)'
    )

    def _get_log_entry_data(self, entry_data: str) -> dict[str, typing.Any]:
        match = self._ENTRY_RE.match(entry_data)
        if not match:
            raise ValueError('Invalid structure')

        return match.groupdict()


@register_logfile_format('jsonlog')
class JsonLogFile(LogFile):
    def _get_log_entry_data(self, entry_data: str) -> dict[str, typing.Any]:
        try:
            return json.loads(entry_data)

        except json.decoder.JSONDecodeError as err:
            raise ValueError(str(err)) from err


def _parse_timestamp(timestamp: str) -> datetime.datetime:
    try:
        return datetime.datetime.strptime(
            timestamp,
            constants.TIMESTAMP_FORMAT,
        )

    except ValueError as err:
        raise ValueError(f'Invalid timestamp "{timestamp}"') from err


_LOG_ENTRY_DATA_SCHEMA = {
    'timestamp': {'type': 'datetime', 'coerce': _parse_timestamp},
    'message_type': {'type': 'string'},
    'destination': {'type': 'integer', 'coerce': int},
    'sender': {'type': 'integer', 'coerce': int},
    'payload': {'type': 'string'},
}
