from __future__ import annotations


LOG_FILE_ENCODING = 'utf-8'

TIMESTAMP_FORMAT = '%Y-%m-%d %H:%M:%S'
