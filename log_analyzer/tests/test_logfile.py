from __future__ import annotations

import datetime
import io
import logging
import os

from parametrization import Parametrization
import pytest

from .. import (
    constants,
    errors,
    logfile,
    types,
)


def test_parse_ping_pong_log(log_path):
    entries = logfile.read_entries(log_path('log_ping_pong.log'))

    assert list(entries) == [
        types.LogEntry(datetime.datetime(2019, 8, 19, 14, 29, 24), 'ping', 1, 2, 'PING'),
        types.LogEntry(datetime.datetime(2019, 8, 19, 14, 29, 25), 'pong', 2, 1, 'PONG'),
        types.LogEntry(datetime.datetime(2019, 8, 19, 14, 30, 24), 'ping', 1, 3, 'PING'),
        types.LogEntry(datetime.datetime(2019, 8, 19, 14, 30, 25), 'pong', 3, 1, 'PONG'),
    ]


@Parametrization.autodetect_parameters()
@Parametrization.case(
    'invalid-timestamp',
    invalid_log_entry='2019-02-29 01:02:03 ping 1 2 PING',
    expected_log_message='Invalid timestamp "2019-02-29 01:02:03"',
)
@Parametrization.case(
    'invalid-structure',
    invalid_log_entry='2019-02-28 01:02:03 ping 1 PING',
    expected_log_message='Invalid structure',
)
def test_invalid_log_entries_in_text_log(
    mocker,
    caplog,
    log_path,
    invalid_log_entry,
    expected_log_message,
):
    log_mock = io.StringIO()
    with open(log_path('log_ping_pong.log'), 'rt', encoding=constants.LOG_FILE_ENCODING) as log:
        log_mock.write(log.read())

    log_mock.write(f'{invalid_log_entry}\n')

    log_mock.seek(0)

    open_mock = mocker.patch('io.open', return_value=log_mock)

    with caplog.at_level(logging.WARNING, logger='log_analyzer'):
        entries = list(logfile.read_entries('test.log'))

    open_mock.assert_called_once_with('test.log', 'rt', encoding=constants.LOG_FILE_ENCODING)

    assert expected_log_message in caplog.text

    assert entries == [
        types.LogEntry(datetime.datetime(2019, 8, 19, 14, 29, 24), 'ping', 1, 2, 'PING'),
        types.LogEntry(datetime.datetime(2019, 8, 19, 14, 29, 25), 'pong', 2, 1, 'PONG'),
        types.LogEntry(datetime.datetime(2019, 8, 19, 14, 30, 24), 'ping', 1, 3, 'PING'),
        types.LogEntry(datetime.datetime(2019, 8, 19, 14, 30, 25), 'pong', 3, 1, 'PONG'),
    ]


def test_parse_json_log(log_path):
    entries = logfile.read_entries(log_path('log_ping_pong.jsonlog'))

    assert list(entries) == [
        types.LogEntry(datetime.datetime(2019, 8, 19, 14, 29, 24), 'ping', 1, 2, 'PING'),
        types.LogEntry(datetime.datetime(2019, 8, 19, 14, 29, 25), 'pong', 2, 1, 'PONG'),
        types.LogEntry(datetime.datetime(2019, 8, 19, 14, 30, 24), 'ping', 1, 3, 'PING'),
        types.LogEntry(datetime.datetime(2019, 8, 19, 14, 30, 25), 'pong', 3, 1, 'PONG'),
    ]


@pytest.mark.parametrize(
    'invalid_log_entry, expected_log_message',
    (
        (
            # pylint: disable=line-too-long
            '{"timestamp": "2019-02-29 01:02:03", "message_type": "pong", "destination": 3, "sender": 1, "payload": "PONG"}',
            'Invalid timestamp "2019-02-29 01:02:03"',
        ),
        (
            '2019-02-01 01:02:03 ping 1 2 PING',
            'Failed to parse line: 2019-02-01 01:02:03 ping 1 2 PING',
        ),
    ),
)
def test_invalid_log_entries_in_json_log(
    mocker,
    caplog,
    log_path,
    invalid_log_entry,
    expected_log_message,
):
    log_mock = io.StringIO()
    with open(log_path('log_ping_pong.jsonlog'), 'rt', encoding=constants.LOG_FILE_ENCODING) as log:
        log_mock.write(log.read())

    log_mock.write(f'{invalid_log_entry}\n')

    log_mock.seek(0)

    open_mock = mocker.patch('io.open', return_value=log_mock)

    with caplog.at_level(logging.WARNING, logger='log_analyzer'):
        entries = list(logfile.read_entries('test.jsonlog'))

    open_mock.assert_called_once_with('test.jsonlog', 'rt', encoding=constants.LOG_FILE_ENCODING)

    assert expected_log_message in caplog.text

    assert entries == [
        types.LogEntry(datetime.datetime(2019, 8, 19, 14, 29, 24), 'ping', 1, 2, 'PING'),
        types.LogEntry(datetime.datetime(2019, 8, 19, 14, 29, 25), 'pong', 2, 1, 'PONG'),
        types.LogEntry(datetime.datetime(2019, 8, 19, 14, 30, 24), 'ping', 1, 3, 'PING'),
        types.LogEntry(datetime.datetime(2019, 8, 19, 14, 30, 25), 'pong', 3, 1, 'PONG'),
    ]


def test_save_as_json_log(mocker):
    log = [
        types.LogEntry(datetime.datetime(2019, 8, 19, 14, 29, 24), 'ping', 1, 2, 'PING'),
        types.LogEntry(datetime.datetime(2019, 8, 19, 14, 29, 25), 'pong', 2, 1, 'PONG'),
        types.LogEntry(datetime.datetime(2019, 8, 19, 14, 30, 24), 'ping', 1, 3, 'PING'),
        types.LogEntry(datetime.datetime(2019, 8, 19, 14, 30, 25), 'pong', 3, 1, 'PONG'),
    ]

    output = io.StringIO()

    logfile.save_as_json_log(log, output)

    mocker.patch('io.open', return_value=output)

    output.seek(0)
    entries = logfile.read_entries('test.jsonlog')

    assert list(entries) == log


@Parametrization.autodetect_parameters()
@Parametrization.default_parameters(
    remove_permissions=False,
)
@Parametrization.case(
    'unknown-log-file-extension',
    # The log_path joins the current directory with the log file's name to form a full path.
    # An existing file in the current directory is used here so that it actually can be read
    # but the extension is unknown
    log_name='__init__.py',
)
@Parametrization.case(
    'unexisting-file',
    log_name='unexisting.log',
)
@Parametrization.case(
    'no-permission',
    log_name='log_ping_pong.log',
    remove_permissions=True,
)
def test_read_entries_errors(
    log_path,
    log_name,
    remove_permissions,
):
    path = log_path(log_name)

    permissions_to_restore = None
    if remove_permissions:
        permissions_to_restore = os.stat(path).st_mode
        os.chmod(path, 0)

    try:
        with pytest.raises(errors.LogAnalyzerFailure):
            list(logfile.read_entries(path))

    finally:
        if permissions_to_restore:
            os.chmod(path, permissions_to_restore)
