from __future__ import annotations

import io
import pytest

from .. import (
    analyze,
    logfile,
)
from .factories import LogEntryFactory


def test_empty_log(
    mocker,
    logfile_open_mock,
):
    stats = analyze.get_stats(mocker.sentinel.log_file_path)

    logfile_open_mock.assert_called_once_with(mocker.sentinel.log_file_path)

    assert stats == analyze.LogStats(
        message_count=0,
        invalid_log_entry_count=0,
        message_counts_by_type={},
    )


def test_log(
    mocker,
    logfile_open_mock,
):
    create_log_entry = LogEntryFactory()

    logfile_open_mock.return_value.__enter__.return_value.__iter__.return_value = [
        create_log_entry(message_type='msg1'),
        create_log_entry(message_type='msg2'),
        create_log_entry(message_type='msg1'),
        create_log_entry(message_type='msg3'),
        create_log_entry(message_type='msg3'),
        create_log_entry(message_type='msg1'),
    ]

    stats = analyze.get_stats(mocker.sentinel.log_file_path)

    logfile_open_mock.assert_called_once_with(mocker.sentinel.log_file_path)

    assert stats == analyze.LogStats(
        message_count=6,
        invalid_log_entry_count=0,
        message_counts_by_type={
            'msg1': 3,
            'msg2': 1,
            'msg3': 2,
        },
    )


@pytest.fixture(name='logfile_open_mock')
def _logfile_open_mock(mocker):
    logfile_mock = mocker.MagicMock(
        name='LogFile',
        spec=logfile.LogFile(io.StringIO()),
    )
    logfile_mock.__iter__.return_value = []
    logfile_mock.invalid_log_entry_count = 0
    logfile_mock.message_counts_by_type = {}

    logfile_open_mock = mocker.MagicMock(name='logfile.open mock return value')
    logfile_open_mock.__enter__.return_value = logfile_mock

    return mocker.patch(
        'log_analyzer.logfile.open',
        return_value=logfile_open_mock,
    )
