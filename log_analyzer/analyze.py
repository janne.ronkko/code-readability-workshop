from __future__ import annotations

import dataclasses
import typing

from . import logfile


@dataclasses.dataclass
class LogStats:
    message_count: int
    invalid_log_entry_count: int
    message_counts_by_type: typing.Dict[str, int]


def get_stats(log_file_path: str) -> LogStats:
    message_count = 0
    message_counts_by_type: dict[str, int] = {}

    with logfile.open(log_file_path) as log:
        for entry in log:
            message_count += 1

            message_counts_by_type[entry.message_type] = message_counts_by_type.get(entry.message_type, 0) + 1

    return LogStats(
        message_count=message_count,
        invalid_log_entry_count=log.invalid_log_entry_count,
        message_counts_by_type=message_counts_by_type,
    )
