from __future__ import annotations

import argparse
import io
import logging
import tempfile

from parametrization import Parametrization
import pytest

from .. import (
    cli,
    constants,
    errors,
)


def test_argparser(mocker):
    parser = cli.create_argparser()

    assert isinstance(parser, argparse.ArgumentParser)

    args = parser.parse_args(['stats', 'foo.log'])
    assert args.main is cli.stats_main
    assert args.log_file == 'foo.log'
    assert args.log_level == 'warning'

    args = parser.parse_args(['--log-level=info', 'stats', 'bar.log'])
    assert args.main is cli.stats_main
    assert args.log_file == 'bar.log'
    assert args.log_level == 'info'

    args = parser.parse_args(['convert', 'test.log'])
    assert args.main is cli.convert_main
    assert args.source_path == 'test.log'
    assert args.output is None
    assert not args.extract_containers
    assert not args.preserve_containers
    assert not args.annotate_extracted
    assert not args.message_types
    assert not args.messagers
    assert args.log_level == 'warning'

    args = parser.parse_args([
        'convert',
        '--filter-messagers=1',
        '--filter-messagers=2',
        '--extract-containers',
        'test.log',
    ])
    assert args.main is cli.convert_main
    assert args.source_path == 'test.log'
    assert args.output is None
    assert args.extract_containers
    assert not args.preserve_containers
    assert not args.annotate_extracted
    assert not args.message_types
    assert args.messagers == {1, 2}
    assert args.log_level == 'warning'

    with pytest.raises(SystemExit):
        args = parser.parse_args([
            'convert',
            '--filter-messagers=a',
            'test.log',
        ])

    parser.print_help = mocker.Mock()
    args = parser.parse_args([])

    with pytest.raises(SystemExit):
        args.main(args)

    parser.print_help.assert_called_once_with()


def test_stats(log_analyzer_main, log_path):
    log_file_path = log_path('log_ping_pong.log')

    stdout = log_analyzer_main(
        'stats',
        source=log_file_path,
    )

    lines = list(stdout.getvalue().split('\n'))
    assert lines == [
        log_file_path,
        '  Messages: 4',
        '  Invalid log entries: 0',
        '  Message type counts:',
        '    ping                      2',
        '    pong                      2',
        '',
    ]


@Parametrization.autodetect_parameters()
@Parametrization.default_parameters(
    write_output_to_file=False,
)
@Parametrization.case(
    'no-extra-args',
    args=(),
    source_log='log_ping_pong_with_container.log',
    expected_log='log_ping_pong_with_container.jsonlog',
)
@Parametrization.case(
    'write-result-to-file',
    write_output_to_file=True,
    args=(),
    source_log='log_ping_pong_with_container.log',
    expected_log='log_ping_pong_with_container.jsonlog',
)
@Parametrization.case(
    'extract-containers',
    args=('--extract-containers',),
    source_log='log_ping_pong_with_container.log',
    expected_log='log_ping_pong_with_container_expanded.jsonlog',
)
@Parametrization.case(
    'extract-containers-and-annotate-extracted',
    args=('--extract-containers', '--annotate-extracted'),
    source_log='log_ping_pong_with_container.log',
    expected_log='log_ping_pong_with_container_expanded_and_annotated.jsonlog',
)
@Parametrization.case(
    'extract-containers-and-preserve-containers',
    args=('--extract-containers', '--preserve-containers'),
    source_log='log_ping_pong_with_container.log',
    expected_log='log_ping_pong_with_container_expanded_and_preserved.jsonlog',
)
@Parametrization.case(
    'extract-container-with-incorrect-payload',
    args=('--extract-containers',),
    source_log='log_container_with_incorrect_inner_message.jsonlog',
    expected_log='log_container_with_incorrect_inner_message_expanded.jsonlog',
)
@Parametrization.case(
    'filter-messagers',
    args=('--filter-messagers=1', '--filter-messagers=2'),
    source_log='log_ping_pong_with_container_expanded_and_preserved.jsonlog',
    expected_log='log_ping_pong_with_container_expanded_and_preserved_filtered1.jsonlog',
)
@Parametrization.case(
    'filter-messages',
    args=('--filter-messages=ping', '--filter-messages=container'),
    source_log='log_ping_pong_with_container_expanded_and_preserved.jsonlog',
    expected_log='log_ping_pong_with_container_expanded_and_preserved_filtered2.jsonlog',
)
def test_convert_ping_pong_with_container_log(
    log_analyzer_main,
    log_path,
    write_output_to_file,
    args,
    source_log,
    expected_log,
):
    output = log_analyzer_main(
        'convert',
        *args,
        source=log_path(source_log),
        write_output_to_file=write_output_to_file,
    )

    with open(log_path(expected_log), 'rt', encoding=constants.LOG_FILE_ENCODING) as expected_log_file:
        assert list(output) == list(expected_log_file)


def test_log_analyzer_failure_handling(
    caplog,
    mocker,
    log_path,
    log_analyzer_main,
):
    mocker.patch(
        'log_analyzer.cli.stats_main',
        side_effect=errors.LogAnalyzerFailure('Test failure'),
    )

    with pytest.raises(SystemExit), caplog.at_level(logging.ERROR):
        log_analyzer_main(
            'stats',
            source=log_path('log_ping_pong.log'),
        )

    assert 'Processing failed: Test failure' in caplog.text


def test_setup_logging(mocker):
    get_logger_mock = mocker.patch('logging.getLogger')
    logger_mock = get_logger_mock.return_value

    cli.setup_logging(mocker.sentinel.loglevel)

    logger_mock.setLevel.assert_called_once_with(mocker.sentinel.loglevel)
    assert not logger_mock.propagate

    logger_mock.addHandler.assert_called_once_with(mocker.ANY)
    handler = logger_mock.addHandler.call_args_list[0][0]
    assert handler is not None


@pytest.fixture(name='log_analyzer_main')
def _log_analyzer_main(capsys, mocker):
    # Configuring logging causes sys.stderr to be closed which in turn causes issues in later tests
    setup_logging_mock = mocker.patch('log_analyzer.cli.setup_logging')

    def main_impl(*args, source, write_output_to_file=False):
        args = args + (source,)

        with tempfile.NamedTemporaryFile(mode='w+t') as tmp_file:
            if write_output_to_file:
                args = args + (f'--output={tmp_file.name}',)

            mocker.patch('sys.argv', ('log_analyzer',) + args)
            cli.main()

            if write_output_to_file:
                assert capsys.readouterr().out == ''
                output = tmp_file.read()

            else:
                assert tmp_file.read() == ''
                output = capsys.readouterr().out

        setup_logging_mock.assert_called_once_with(mocker.ANY)

        return io.StringIO(output)

    return main_impl
