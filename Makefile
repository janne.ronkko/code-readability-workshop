PYTHON ?= python

PYTHON_PACKAGE_NAME := $(shell python setup.py --name | sed s:-:_:g)
CDG_BACKEND_VERSION := $(shell python setup.py --version)

SHELL := bash

VENV_DIR ?= $(shell pwd)/venv
VENV_BIN_DIR := ${VENV_DIR}/bin/
REQUIREMENTS_DIR := requirements/

PYTHON_PACKAGES := log_analyzer
SOURCES_FOR_STATIC_ANALYZERS := setup.py ${PYTHON_PACKAGES}


default: venv

.PHONY:
venv: requirements/development.txt | ${VENV_BIN_DIR}python
	${VENV_BIN_DIR}python -m pip install -r $<
	${VENV_BIN_DIR}python setup.py develop

.PHONY:
mypy:
	${VENV_BIN_DIR}mypy ${SOURCES_FOR_STATIC_ANALYZERS}

.PHONY:
pycodestyle:
	${VENV_BIN_DIR}pycodestyle ${SOURCES_FOR_STATIC_ANALYZERS}

.PHONY:
pylint:
	${VENV_BIN_DIR}pylint -j 0 ${SOURCES_FOR_STATIC_ANALYZERS}

.PHONY:
pytest:
	${VENV_BIN_DIR}pytest \
  --cov-report= \
  --cov log_analyzer \
  -vv \
  -x \
  ${PYTHON_PACKAGES} \

.PHONY:
coverage-text-report:
	${VENV_BIN_DIR}python -m coverage report

.PHONY:
coverage-html-report:
	${VENV_BIN_DIR}python -m coverage html

coverage-xml-report:
	${VENV_BIN_DIR}python -m coverage xml

WHEEL_FILENAME := ${PYTHON_PACKAGE_NAME}-${CDG_BACKEND_VERSION}-py3-none-any.whl

wheel: dist/${WHEEL_FILENAME}

dist/${WHEEL_FILENAME}: .git/HEAD setup.py $(shell find ${PYTHON_PACKAGES} -name '*.py')
	python setup.py bdist_wheel

PIP_COMPILE := ${VENV_BIN_DIR}pip-compile --no-header --emit-trusted-host --annotate --generate-hashes --reuse-hashes

.PHONY:
update-dependencies: ${VENV_BIN_DIR}pip-compile
	${PIP_COMPILE} --output-file ${REQUIREMENTS_DIR}runtime.txt setup.py
	${PIP_COMPILE} --output-file ${REQUIREMENTS_DIR}development.txt ${REQUIREMENTS_DIR}runtime.txt ${REQUIREMENTS_DIR}development.in

.PHONY:
upgrade-dependencies: ${VENV_BIN_DIR}pip-compile
	${PIP_COMPILE} --upgrade --output-file ${REQUIREMENTS_DIR}runtime.txt setup.py
	${PIP_COMPILE} --upgrade --output-file ${REQUIREMENTS_DIR}development.txt ${REQUIREMENTS_DIR}runtime.txt ${REQUIREMENTS_DIR}development.in

${VENV_BIN_DIR}pip-compile: ${VENV_BIN_DIR}python
	${VENV_BIN_DIR}pip install pip-tools

${VENV_BIN_DIR}python:
	${PYTHON} -m venv ${VENV_DIR}
